import BusinessItem, { parse } from "../entities/BusinessItem";

export function fetchBusinessItems(): Promise<BusinessItem[]> {
  return new Promise<BusinessItem[]>((resolve, reject) => {
    fetch("https://api.jsonbin.io/b/6177e9399548541c29c8c0f5")
      .then((res) => res.json())
      .then((response) => {
        if (Array.isArray(response)) {
          resolve(response.map(parse));
        } else {
          reject(response.message);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
}
