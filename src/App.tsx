import Container from "@mui/material/Container";
import React from "react";
import { Route, Switch } from "react-router-dom";
import "./App.css";
import Header from "./components/Header";
import ListPage from "./components/ListPage";
import OverviewPage from "./components/OverviewPage";
import { BASE_PATH, OVERVIEW_PATH } from "./routes";

function App() {
  return (
    <div className="container">
      <Header />
      <Container maxWidth="lg" className="sub-container">
        <Switch>
          <Route exact path={BASE_PATH} component={ListPage} />
          <Route path={OVERVIEW_PATH} component={OverviewPage} />
        </Switch>
      </Container>
    </div>
  );
}

export default App;
