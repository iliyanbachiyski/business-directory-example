import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Typography from "@mui/material/Typography";
import React, { useCallback, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../hooks";
import { OVERVIEW_PATH } from "../routes";
import { fetchBusinessItems } from "../slices";
import "../App.css";

const ListPage: React.FC = () => {
  const dispatch = useAppDispatch();
  const history = useHistory();
  const items = useAppSelector((state) => Object.values(state.business.byId));
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");

  useEffect(() => {
    if (!items.length) {
      dispatch(fetchBusinessItems())
        .then(() => {
          setLoading(false);
        })
        .catch((error) => {
          if (typeof error === "string") {
            setError(error);
          } else {
            setError("Something went wrong!");
          }
          setLoading(false);
        });
    }
  }, []);

  const handleItemClick = useCallback((id: string) => {
    history.push(OVERVIEW_PATH.replace(":id", id));
  }, []);

  if (error) {
    return (
      <Typography variant="h5" textAlign="center" color="error">
        Error: {error}
      </Typography>
    );
  }

  if (!items.length) {
    if (loading) {
      return (
        <Box display="flex" justifyContent="center">
          <CircularProgress size={80} />
        </Box>
      );
    }
    return (
      <Typography variant="h5" textAlign="center">
        No items fetched!
      </Typography>
    );
  }

  return (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>
              <Typography variant="body1" color="secondary">
                <b>NAME</b>
              </Typography>
            </TableCell>
            <TableCell>
              <Typography variant="body1" color="secondary">
                <b>DESCRIPTION</b>
              </Typography>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {items.map((item) => (
            <TableRow
              className="tableRow"
              hover
              key={item.id}
              onClick={() => handleItemClick(item.id)}
            >
              <TableCell>{item.name}</TableCell>
              <TableCell>{item.description}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default ListPage;
