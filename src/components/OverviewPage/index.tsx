import { Typography } from "@mui/material";
import CircularProgress from "@mui/material/CircularProgress";
import Box from "@mui/system/Box";
import React, { useEffect, useState } from "react";
import { RouteComponentProps } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../hooks";
import {
  fetchBusinessItems,
  selectBusinessItemById,
  selectNearbyItems,
} from "../../slices";
import "./style.css";

const OverviewPage: React.FC<RouteComponentProps<{ id: string }>> = ({
  match,
}) => {
  const dispatch = useAppDispatch();
  const item = useAppSelector((state) =>
    selectBusinessItemById(state, match.params.id)
  );
  const nearbyItems = useAppSelector((state) =>
    selectNearbyItems(state, match.params.id)
  );
  const [fetching, setFetching] = useState(!item);
  const [error, setError] = useState("");

  useEffect(() => {
    if (!item) {
      dispatch(fetchBusinessItems())
        .then(() => {
          setFetching(false);
        })
        .catch((error) => {
          if (typeof error === "string") {
            setError(error);
          } else {
            setError("Something went wrong!");
          }
          setFetching(false);
        });
    }
  }, []);

  if (error) {
    return (
      <Typography variant="h5" textAlign="center" color="error">
        Error: {error}
      </Typography>
    );
  }

  if (!item) {
    if (fetching) {
      return (
        <Box
          width="100%"
          height="100%"
          display="flex"
          alignItems="center"
          justifyContent="center"
        >
          <CircularProgress size={80} />
        </Box>
      );
    }
    return <div>Item not found!</div>;
  }

  return (
    <Box display="flex" flexDirection="column">
      <img className="company-logo" src={item.image} alt="company-logo" />
      <Box display="flex" width="90%" margin="auto">
        <Box display="flex" justifyContent="space-between" flex={1} padding={2}>
          <Box display="flex" flexDirection="column" flex={1}>
            <Typography variant="h5" gutterBottom>
              Address
            </Typography>
            <Typography variant="body2" color="GrayText">
              {item.address.number} {item.address.street} {item.address.city}
              <br />
              {item.address.country} {item.address.zip}
            </Typography>
          </Box>
          <Box display="flex" flexDirection="column" flex={1}>
            <Typography variant="h5" gutterBottom>
              Contact
            </Typography>
            <Typography variant="body2" color="GrayText">
              {item.phone}
            </Typography>
            <Typography variant="body2" color="GrayText">
              {item.email}
            </Typography>
          </Box>
        </Box>
        <Box
          display="flex"
          flexDirection="column"
          flex={1}
          padding={2}
          bgcolor="white"
        >
          <Typography variant="h5" gutterBottom>
            Nearby Places
          </Typography>
          <Box display="flex" flexDirection="column">
            {nearbyItems.map((el) => {
              return <NearbyPlaceItem key={el} placeId={el} />;
            })}
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

interface NearbyPlaceItemProps {
  placeId: string;
}

const NearbyPlaceItem: React.FC<NearbyPlaceItemProps> = ({ placeId }) => {
  const item = useAppSelector((state) =>
    selectBusinessItemById(state, placeId)
  );
  return (
    <Box display="flex" padding={1} bgcolor="whitesmoke" marginBottom={1}>
      <Typography flex={1} variant="body2" color="GrayText" noWrap>
        {item.name}
      </Typography>
      <Typography flex={3} variant="body2" color="GrayText" noWrap>
        {item.address.number} {item.address.street} {item.address.city},
        {item.address.country} {item.address.zip}
      </Typography>
    </Box>
  );
};

export default OverviewPage;
