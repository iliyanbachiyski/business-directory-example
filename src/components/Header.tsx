import Box from "@mui/material/Box";
import React from "react";
import logo from "../logo.svg";

function Header() {
  return (
    <Box className="header" display="flex" justifyContent="center">
      <img src={logo} alt="logo" />
    </Box>
  );
}

export default Header;
