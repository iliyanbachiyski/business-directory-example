interface BusinessItem {
  id: string;
  name: string;
  description: string;
  phone: string;
  image: string;
  email: string;
  address: {
    number: Number;
    street: string;
    zip: string;
    city: string;
    country: string;
  };
}

export const parse = function (data: any): BusinessItem {
  return {
    id: data.id,
    name: data.name,
    description: data.description,
    phone: data.phone,
    image: data.image,
    email: data.email,
    address: {
      number: Number(data.address?.number),
      street: data.address ? data.address.street : "N/A",
      zip: data.address ? data.address.zip : "N/A",
      city: data.address ? data.address.city : "N/A",
      country: data.address ? data.address.country : "N/A",
    },
  };
};

export default BusinessItem;
