import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { fetchBusinessItems as fetchBusinessItemsApi } from "../api";
import BusinessItem from "../entities/BusinessItem";
import { AppState, AppThunk } from "../store";

type FetchItemsPayload = { items: BusinessItem[] };

export interface BusinessState {
  byId: { [key: string]: BusinessItem };
}

const initialState: BusinessState = {
  byId: {},
};

export const fetchBusinessItems =
  (): AppThunk<Promise<void>> => (dispatch, _) => {
    return fetchBusinessItemsApi().then((items) => {
      dispatch(setBusinessItems({ items }));
    });
  };

export const businessSlice = createSlice({
  name: "business",
  initialState,
  reducers: {
    setBusinessItems: (state, action: PayloadAction<FetchItemsPayload>) => {
      const { items } = action.payload;
      items.forEach((item) => {
        state.byId[item.id] = { ...item };
      });
    },
  },
});

export const { setBusinessItems } = businessSlice.actions;

export const selectBusinessItemById = (state: AppState, placeId: string) =>
  state.business.byId[placeId];

export const selectNearbyItems = (state: AppState, placeId: string) => {
  const place = state.business.byId[placeId];
  return Object.values(state.business.byId)
    .filter((item) => item.address.city === place.address.city)
    .map((el) => el.id);
};

export default businessSlice.reducer;
